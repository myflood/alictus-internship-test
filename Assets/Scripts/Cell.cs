﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
    public float speed = 6;
    private float x;
    private float x2;
    private float y;
    private float y2;
    private float i;
    private float j;
    private int prev;
    public GameObject square;
    private float count;
    public Text text;
    private Vector3 v3;

    private void Start()
    {
        count = 16;
        text.text = "Number of cubes: " + count.ToString();
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && prev != 1)
        {
            prev = 0;
            transform.position += Vector3.left * speed * Time.deltaTime;
            x = transform.position.x;
        }
        else if (Input.GetKey(KeyCode.RightArrow) && prev !=0)
        {
            prev = 1;
            transform.position += Vector3.right * speed * Time.deltaTime;
            x2 = transform.position.x;


        }
        else if (Input.GetKey(KeyCode.UpArrow) && prev != 3)
        {
            prev = 2;
            transform.position += Vector3.up * speed * Time.deltaTime;
            y2 = transform.position.y;
           
        }
        else if (Input.GetKey(KeyCode.DownArrow) && prev != 2)
        {
            prev = 3;
            transform.position += Vector3.down * speed * Time.deltaTime;
            y = transform.position.y;

        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "base")
        {
            float copy = y;
            float copy2 = x;
            for (x = copy2; x <= x2 + 2; x++)
                for (y = copy; y <= y2; y++)
                    Instantiate(square, new Vector3((int)x - (0.5f), (int)y + (0.5f)), Quaternion.identity);
            count += (Mathf.Abs(x2 - copy2)) * (Mathf.Abs(y2 - copy));
            text.text = "Number of cubes: " + ((int)count).ToString();
        }
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "base")
        {
            x = transform.position.x;
            x2 = x;
            y = transform.position.y;
            y2 = y;
        }
    }
}